check containers (creation, pods, status): sudo kubectl get all -n project-k8s

lauching the kubernetes dashboard: sudo minikube dashboard
authorize external connection on the dashboard on a different terminal: sudo kubectl proxy --address='0.0.0.0' --disable-filter=true

On the browser copy and paste the url provided by the first lauch command